# Prueba técnica Aplicación Flutter.

## Ejercicio 1

Crea una aplicación en flutter que sea una réplica de la pantalla de home de la aplicación de BestLife. Debe ser identica a nivel visual y comportamiento (scroll vertical y carousel de clases). No hay que hacer consultas de ningún tipo al API.

![Home](screenshots/home.png)

**Requisitos:**

- Debe contener menu de navegación inferior.
- Sólo debe crearse la Home y el resto deben ser páginas vacías.
- Para las imágenes de las clases del carrusel puede usarse la imágen `training-class-image-m.png` y `training-class-graph.png` como placeholder.

![Training Class](images/training-class-image-m.png)
![Training Class Graph](images/training-class-graph.png)

- Al pulsar una de las clases del carrusel de clases debe aparecer otra ventana con el título de la clase en la barra superior y el botón volver.

## Ejercicio 2

Modifica la página vacía para que muestre un listado de las últimas clases de Bestcycling Life utilizando el api de Bestcycling. No hace falta filtrar por categorías.

![Search](screenshots/search.png)

**Requisitos:**

- Debe obtener los datos del API y construir un listado similar al listado de la aplicación actual. Se puede acceder al listado en la pestaña "Buscar > Buscar clases > [Categoría]".
- No es necesario que a nivel de diseño sea identica, pero sí debe contener la información mostrada, cuyos datos se pueden obtener a través del API.
- No es necesario añadir el encabezado con el año.
- Los puntos de la zona inferior izquierda son, por orden: Resistencia, Fuerza, Flexibilidad y Mente.

Para obtener las últimas clases publicadas en Bestcycling hay que lanzar una petición GET al siguiente endpoint:

https://apiv2.bestcycling.es/api/v2/training_classes?&include=trainer

y enviando el siguiente encabezado de autenticación `Authorization`:

    Bearer <API_KEY>

Ejemplo de petición con cURL:
curl --location --request GET 'https://apiv2.bestcycling.es/api/v2/training_classes?&include=trainer' \
--header 'Content-Type: application/vnd.api+json' \
--header 'authorization: Bearer <API_KEY>' \
--header 'X-APP-ID: Flutter'

### API:

Documentación de tipos usados en la respuesta:

#### TrainingClass

- **category: String**
  Categoría a la que pertenece la clase.

- **category_nr: Int**
  ID de categoría de las clase

- **cover: String**
  Imágen de clase en listados.

- **graph: String**
  Imagen de la gráfica

- **delay_seconds: Int**
  Tiempo de espera antes de empezar la clase.

- **duration_training: Int**
  Duración de entrenamiento en segundos, sin contar introducción y vuelta a la calma.

- **graph: String**
  Imágen de gráfica de la clase.

- **id: ID**
  Identificador único de la clase.

- **level: String**
  Nivel de la clase. [Principiante|Medio|Avanzado]

- **level_nr: Int**
  ID de nivel de la clase. 10 => Principiante, 20 => Medio, 30 => Avanzado

- **life_flexibility_points: Int**
  Puntos de flexibilidad de la clase.

- **life_force_points: Int**
  Puntos de fuerza de la clase.

- **life_mind_points: Int**
  Puntos de mente de la clase.

- **life_resistance_points: Int**
  Puntos de resistencia de la clase.

- **short_title: String**
  Nombre de clase abreviado

- **subtitle: String**
  Subtítulo de la clase.

- **title: String**
  Nombre de la clase.
- **content: String**
  Descripción de la clase.

- **training_type: String**
  Tipo de entrenamiento de la clase.

- **updated_at_timestamp: Int**
  Timestamp de última modificación de la clase en formato Unix.

- **relationships**
  Relaciones con otras clases

  - **trainer: trainer**
    Instructor de la clase. Los datos se obtienen en "included"

#### trainer

- **full_name: String**
  Nombre completo del instructor.

## Recursos

- **Flutter SDK:**
  https://flutter.dev/

- **Curso `Build Native Mobile Apps with Flutter` de Google**
  https://eu.udacity.com/course/build-native-mobile-apps-with-flutter--ud905

- **Json Api**
  https://jsonapi.org/
